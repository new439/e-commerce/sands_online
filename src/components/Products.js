import React from 'react'


const Products = (props) => {
    const { productItems, onAdd } = props;
  return (  
      <div className='products'>
        {productItems.map((productItems) => (
          <div className='product-card'>
            <div>
              <img className='product-img' src={productItems.image} alt={productItems.name}/>
            </div>
            <div>
              <div>
                <h3 className='product-name'>{productItems.name}</h3>
              </div>
              <div className='product-des'>{productItems.description}</div>
              <div className='product-price'>Php{productItems.price}</div>
              <div>
                <button className='add-to-cart-button' onClick={()=>onAdd(productItems)}>Add To Cart</button>
              </div>
            </div>

          </div>
        ))}

      </div>
  )
}

export default Products