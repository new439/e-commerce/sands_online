import React from 'react';
import { Link } from 'react-router-dom';
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Header = (props) => {
  const {countCartItems} = props;
    return(
        <div className='Header'>
          <div className='nav-brand'>
            <h2>S & S online</h2>               
          </div>
          <div className='header-links'>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/products">Products</Link>
              </li>
              <li>
                <Link to="/login">Login</Link>
              </li>
              <li>
                <Link to="/signin">Signin</Link>
              </li>
              <li>
                <Link to="/cart" className='cart'><FontAwesomeIcon icon={faShoppingCart} ></FontAwesomeIcon>
                {' '}
                 { countCartItems ? (
                  <button className='badge'>{countCartItems}</button>
                 ) : (
                  ''
               )}
                
                </Link>
              </li>
              <li>
                <Link to="/">Logout</Link>
              </li>
            </ul>
          </div>
        </div>
    )
}
export default Header;