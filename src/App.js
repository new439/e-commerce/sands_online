import './App.css';
import data from './mockData/data';
import Home from './components/Home';
import Header from './components/Header';
import Cart from './components/Cart';
import Products from './components/Products';
import ErrorPage from './pages/ErrorPage';
import { useState } from 'react';
import { BrowserRouter, Routes, Route  } from 'react-router-dom';


function App() {

  const {productItems} = data;
  const [cartItems, setCartItems] = useState([]);


  const onAdd = (productItems) => {
    const exist = cartItems.find(x => x.id === productItems.id)
    if(exist){
      setCartItems(cartItems.map(x => x.id === productItems.id ? {...exist, qty: exist.qty + 1}: x))
    }else{
      setCartItems([...cartItems, {...productItems, qty: 1}])
    }
  };

  const onRemove = (productItems) => {
    const exist = cartItems.find((x) => x.id === productItems.id);
    if(exist.qty === 1){
      setCartItems(cartItems.filter((x) => x.id !== productItems.id))
    }else{
      setCartItems(cartItems.map(x => x.id === productItems.id ? {...exist, qty: exist.qty - 1}: x))
    }
  }

  const onClear = () => {
    setCartItems([]);
  }

  return (
    <BrowserRouter >
      < Header countCartItems={cartItems.length}/>    
        <Routes>
          <Route path="/" element={ <Home />} />
          <Route path="/products" element={<Products productItems={productItems} onAdd={onAdd}/>} />
          <Route path="/cart" element={<Cart cartItems={cartItems} onAdd={onAdd} onRemove={onRemove} onClear={onClear}/>}/>
          <Route path="*" element={<ErrorPage/>}/>
        </Routes> 
        
    </BrowserRouter>
  );
}

export default App;


